var image = {};
const child_process = require('child_process');
const spawn = child_process.spawn;

//magick.exe -define stream:buffer-size=0 .\background.png -resize 200x200 png:-

image.spawnResizeImg  = function (path,file,size)
{
    var args = ['-define','stream:buffer-size=0', file,"-resize",size,"png:-"]									
	var magick = spawn(path, args);
										
	console.log('Spawning ' + path + ' ' + args.join(' '));
	
	magick.stderr.on('data', function (data) {
    console.log('flac stderr: ' + data);
  });
    
  return magick;
}

module.exports = image;