const Mime = require('mime');
const Fs = require('fs');
const Crypto = require('crypto');
const Path = require('path')

// var Bdd = require('./bdd.js');
var Image = require('./image');
var Config = require('./config');
var Audio = require('./audio');


var File = {}

//effectue <convertionFunc> sur <path> si il est est pas en cache, renvoie le resultat de la convertion ou le cache.
// dir : dossier ou est enregistré le cache
// res : res de la requete http
File.cache = function(path,res,dir,convertionFunc)
{
    var mimeType = Mime.lookup(path);
    var hash = Crypto.createHash('sha256').update(path).digest('hex') + Path.extname(path);
    var miniature = Config.cache + "/"+dir+"/" + hash;                 
    
    Fs.stat(miniature,
        function(err,stats)
        {
            if(err)//if miniature doesn't exist we create it
            {
                var fileOut = Fs.createWriteStream(miniature, { flags: 'w' });
                convertionFunc.stdout.pipe(fileOut);
                convertionFunc.stdout.pipe(res);                
                return;
            }
            
            if(stats.isFile())//if miniature exist we send it
            {
                res.sendFile(miniature);
            }
            else//if miniature is not a file, error !
            {
                res.sendStatus(500);
            }
        }
    )
        
}


File.getFile = function(path,res)
{
    
    Fs.stat(path,
        function(err,stats)
        {
            if(err)
            {
                console.log(path+" not found");
                res.sendStatus(404);
                
                return;
            }

            if(stats.isFile())
            {
                var mimeType = Mime.lookup(path);
                
                
                if(Config.mimeImages.indexOf(mimeType) > -1)//pour les images
                {
                    var magick = Image.spawnResizeImg(Config.imageMagick,path,"1000x1000");
                    File.cache(path,res,'max',magick);
                }
                else if(mimeType ==  "audio/x-flac")
                {
                    res.contentType('audio/mpeg');
                    var lame = Audio.spawnLame(Config.lame,"320");
                    //on met son stdout sur la sortie du serveur
                    File.cache(path,res,'mp3-320',lame);
                    //on lance flac
                    var flac = Audio.spawnFlac(Config.flac,path);
                    //on 'pipe' flac vers lame
                    flac.stdout.pipe(lame.stdin);
                }
                else 
                {
                    
                    res.sendFile(path);
                }    
                
            }
            else
            {
                res.sendStatus(400);
                return;
            }
        }
    
    ) 
    
}

File.original = function(path,res)
{
    Fs.stat(path,
        function(err,stats)
        {
            if(err)
            {
                res.sendStatus(404);
                return;
            }

            if(stats.isFile())
            {
                res.sendFile(path);
                return
            }
            else
            {
                res.sendStatus(400);
                return;
            }
        }
    )
}


File.miniature = function(path,res)
{
    Fs.stat(path,
        function(err,stats)
        {
            if(err)
            {
                console.log("toto");
                res.sendStatus(404);
                
                return;
            }

            if(stats.isFile())
            {
                var mimeType = Mime.lookup(path);             
                if(Config.mimeImages.indexOf(mimeType) > -1)//pour les Images
                {
                    var magick = Image.spawnResizeImg(Config.imageMagick,path,"200x200");
                    File.cache(path,res,'min',magick);                    
                }
                else 
                {                            
                    File.genIcone(path,res);
                }    
                
            }
            else
            {
                res.sendStatus(400);
                return;
            }
        }
    
    ) 
}







//verifie que les paramètre sont bons pour l'optention d'un fichier/dossier
// si pas de params renvoie false
// sinon passe le chemin du fichier/dossier à <action> ou renvoie un code d'erreur http
File.doFile = function (req,action,res)
{
    if(req.query.path != null)
    {
        if(req.query.biblio != null && req.query.path.indexOf("..") == -1)
        {
            let biblio = req.query.biblio;
            if(Config.biblios[biblio] != null)
            {
                let path = Config.biblios[biblio] + "/" + req.query.path;
                action(path,res);
                return true;
            }            
            else
            {
                res.sendStatus(404);//si biblio inconnue 
                return true
            }
        }
        else
        {
            res.sendStatus(400); //si path mais pas biblio
            return true
        }
    }
    else
    {
        return false;
    }
}


File.genIcone = function(path,res)
{
    var mimeType = Mime.lookup(path);                        
    res.sendFile(config.static+"/inconnu.ico");
}


module.exports = File;
