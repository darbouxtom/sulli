//function to convert audio file

var Child_process = require('child_process');

var Spawn = Child_process.spawn;

var Audio = {};

//path : path of Lame
//quality : kbps (320 = high/good quality)
//spawn lame on the stdin and return mp3 on the stdout
Audio.spawnLame = function(path,quality) {
  var args = ['-b',quality, '-', '-']
											
	var lame = Spawn(path, args);
										
	console.log('Spawning '+path+' ' + args.join(' '));
	
	lame.stderr.on('data', function (data) {
    console.log('lame stderr: ' + data);
  });
    
  return lame;
}

//lance flac sur <file> et retourne le flux dans le stdin
//  path:path de flac
Audio.spawnFlac = function(path,file)
{
    var args = ['-c','-d', file]
											
	var flac = Spawn(path, args);
										
	console.log('Spawning ' + path + ' ' + args.join(' '));
	
	flac.stderr.on('data', function (data) {
    console.log('flac stderr: ' + data);
  });
    
  return flac;
}

module.exports = Audio;