Sulli - music version
=====================

presentation
------------

Sulli aims to be a Rest API to serve files with authentication.

This branch is a stable lightweight version for eargasmer project whithout authentication and without need of bdd.

install
-------

```
npm install
```

and edit config.js with your needs.

launch server
-------------

```sh
node server.js
```