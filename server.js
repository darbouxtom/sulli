/*
Test en devel on windows : 
http://localhost:3000/File?path=Capture.png&biblio=phone => image miniature
http://localhost:3000/File?path=01.flac&biblio=phone => adele mp3

*/


const Express = require('express')
const Fs = require('fs');
const Mime = require('mime');
const Child_process = require('child_process');
const app = Express();
const BodyParser = require('body-parser')

var jsonParser = BodyParser.json()

// create application/x-www-form-urlencoded parser
var urlencodedParser = BodyParser.urlencoded({ extended: false })   


var Config = require('./config');
var Audio = require('./audio');
var Image = require('./image');
var File = require('./file');

app.get('/', 
    function (req, res) 
    {
        res.send("<h1>welcome to sulli version music!</h1>");
    })

//info sur le fichier
app.head("/File",
    function(req,res)
    {
        res.sendStatus(404);
    }
)

//envoi du fichier tel quel
app.get("/File/original",
    function(req,res)
    {
        if(! File.doFile(req,File.original,res))
        {
            res.sendStatus(400);    
        }  
    }
)

app.get("/File/miniature",
    function(req,res)
    {
        if(! File.doFile(req,File.miniature,res))
        {
                res.sendStatus(400);  
        }
    }

)






//convertion puis envoi du fichier
app.get("/File",
    function(req,res)
    {
        if(! File.doFile(req,File.getFile,res))
        {
            res.sendStatus(400);  
        }   
    }
)
//no static for an api nothing is really public
//app.use("/static",Express.static('static')) 


app.listen(Config.port, function () {
    /*
    //connect to bdd
    Bdd.connection.connect(function(err) {
        if (err) {
          console.error('error connecting bdd: ' + err.stack);
          process.exit(-1);
        }
       
        console.log('Sulli is connected to bdd weloveminitel as id ' + Bdd.connection.thradIed);
      });

	setInterval(function () {
    		Bdd.connection.query('SELECT 1');
	}, 10000);
    */
    console.log('Sulli listening on port ' + Config.port + ' (et pas toi :p)!')
})
